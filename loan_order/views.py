from loan_order.models import LoanOrder
from loan_order.serializers import PostLoanOrderSerializer, GetLoanOrderSerializer

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response


@api_view(['GET', 'POST'])
def get_post_loan_order(request):

    if request.method == 'GET':
        loan_orders = LoanOrder.objects.all()
        serializer = PostLoanOrderSerializer(loan_orders, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = PostLoanOrderSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'id': serializer.data.get('id')}, status=status.HTTP_201_CREATED)
        return Response({'errors': [serializer.errors]}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def get_loan_order(request, pk):
    try:
        loan_order = LoanOrder.objects.get(pk=pk)
    except LoanOrder.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    serializer = GetLoanOrderSerializer(loan_order)
    return Response(serializer.data)
