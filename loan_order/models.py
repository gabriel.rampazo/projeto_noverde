import uuid

from django.db import models
from django.core.exceptions import ValidationError

from validate_docbr import CPF

INSTALLMENTS = [
    (6, '6x'),
    (9, '9x'),
    (12, '12x')
]

STATUS = [
    ('processing', 'Processing'),
    ('completed', 'Completed')
]

RESULT = [
    ('approved', 'Approved'),
    ('refused', 'Refused')
]

REFUSED_POLICY = [
    ('age', 'Age'),
    ('score', 'Score'),
    ('commitment', 'Commitment')
]


def validate_cpf(value):
    cpf = CPF()
    if cpf.validate(value):
        return value
    raise ValidationError("Número de CPF inválido.")


class LoanOrder(models.Model):
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=255)
    cpf = models.CharField(max_length=14, validators=[validate_cpf])
    birthdate = models.DateField()
    amount = models.DecimalField(max_digits=6, decimal_places=2)
    terms = models.IntegerField(choices=INSTALLMENTS)
    income = models.DecimalField(max_digits=6, decimal_places=2)
    status = models.CharField(max_length=14, choices=STATUS, default='processing')
    result = models.CharField(max_length=14, choices=RESULT, null=True, blank=True)
    refused_policy = models.CharField(max_length=14, choices=REFUSED_POLICY, null=True, blank=True)
    amount_approved = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    terms_approved = models.IntegerField(choices=INSTALLMENTS, null=True, blank=True)

    def __str__(self):
        return self.name
