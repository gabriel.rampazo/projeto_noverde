from django.contrib import admin

from .models import LoanOrder

admin.site.register(LoanOrder)