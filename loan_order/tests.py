from rest_framework.test import APITestCase
from rest_framework import status


class RegistrationTestCase(APITestCase):

    def test_loan_order_ok(self):
        data = {
            "name": "João da Silva",
            "cpf": "221.245.180-62",
            "birthdate": "1988-05-15",
            "amount": 2000.00,
            "terms": 12,
            "income": 2500.00
        }
        response = self.client.post("/loan/", data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_loan_order_refused_name(self):
        data = {
            "name": "",
            "cpf": "221.245.180-62",
            "birthdate": "1988-05-15",
            "amount": 2000.00,
            "terms": 12,
            "income": 2500.00
        }
        response = self.client.post("/loan/", data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_loan_order_refused_cpf(self):
        data = {
            "name": "João da Silva",
            "cpf": "",
            "birthdate": "1988-05-15",
            "amount": 2000.00,
            "terms": 12,
            "income": 2500.00
        }
        response = self.client.post("/loan/", data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {
            "name": "João da Silva",
            "cpf": "221.245.180-623",
            "birthdate": "1988-05-15",
            "amount": 2000.00,
            "terms": 12,
            "income": 2500.00
        }
        response = self.client.post("/loan/", data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {
            "name": "João da Silva",
            "cpf": "221.245.180-63",
            "birthdate": "1988-05-15",
            "amount": 2000.00,
            "terms": 12,
            "income": 2500.00
        }
        response = self.client.post("/loan/", data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_loan_order_refused_birthdate(self):
        data = {
            "name": "João da Silva",
            "cpf": "221.245.180-62",
            "birthdate": "",
            "amount": 2000.00,
            "terms": 12,
            "income": 2500.00
        }
        response = self.client.post("/loan/", data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {
            "name": "João da Silva",
            "cpf": "221.245.180-62",
            "birthdate": "1988-05-32",
            "amount": 2000.00,
            "terms": 12,
            "income": 2500.00
        }
        response = self.client.post("/loan/", data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {
            "name": "João da Silva",
            "cpf": "221.245.180-62",
            "birthdate": "1988-13-15",
            "amount": 2000.00,
            "terms": 12,
            "income": 2500.00
        }
        response = self.client.post("/loan/", data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {
            "name": "João da Silva",
            "cpf": "221.245.180-62",
            "birthdate": "15-05-1988",
            "amount": 2000.00,
            "terms": 12,
            "income": 2500.00
        }
        response = self.client.post("/loan/", data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {
            "name": "João da Silva",
            "cpf": "221.245.180-62",
            "birthdate": "05-15-1988",
            "amount": 2000.00,
            "terms": 12,
            "income": 2500.00
        }
        response = self.client.post("/loan/", data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_loan_order_refused_amount(self):
        data = {
            "name": "João da Silva",
            "cpf": "221.245.180-62",
            "birthdate": "1988-05-15",
            "amount": "",
            "terms": 12,
            "income": 2500.00
        }
        response = self.client.post("/loan/", data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {
            "name": "João da Silva",
            "cpf": "221.245.180-62",
            "birthdate": "1988-05-15",
            "amount": 999.00,
            "terms": 12,
            "income": 2500.00
        }
        response = self.client.post("/loan/", data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {
            "name": "João da Silva",
            "cpf": "221.245.180-62",
            "birthdate": "1988-05-15",
            "amount": 4001.00,
            "terms": 12,
            "income": 2500.00
        }
        response = self.client.post("/loan/", data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_loan_order_refused_terms(self):
        data = {
            "name": "João da Silva",
            "cpf": "221.245.180-62",
            "birthdate": "1988-05-15",
            "amount": 2000.00,
            "terms": "",
            "income": 2500.00
        }
        response = self.client.post("/loan/", data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        data = {
            "name": "João da Silva",
            "cpf": "221.245.180-62",
            "birthdate": "1988-05-15",
            "amount": 2000.00,
            "terms": 2,
            "income": 2500.00
        }
        response = self.client.post("/loan/", data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_loan_order_refused_income(self):
        data = {
            "name": "João da Silva",
            "cpf": "221.245.180-62",
            "birthdate": "1988-05-15",
            "amount": 2000.00,
            "terms": 12,
            "income": ""
        }
        response = self.client.post("/loan/", data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {
            "name": "João da Silva",
            "cpf": "221.245.180-62",
            "birthdate": "1988-05-15",
            "amount": 2000.00,
            "terms": 12,
            "income": -1.00
        }
        response = self.client.post("/loan/", data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
