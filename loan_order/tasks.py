from __future__ import absolute_import, unicode_literals

import json
import requests

from celery import shared_task
from datetime import date
from dateutil.relativedelta import relativedelta

from .models import LoanOrder


url_score = "https://challenge.noverde.name/score"
url_commitment = "https://challenge.noverde.name/commitment"
headers = {'x-api-key': 'z2qcDsl6BK8FEPynp2ND17WvcJKMQTpjT5lcyQ0d'}


@shared_task()
def credit_motor():
    lo_obj = LoanOrder.objects.filter(status='processing').first()

    if lo_obj:
        
        cpf = lo_obj.cpf
        terms = lo_obj.terms
        amount = lo_obj.amount
        income = lo_obj.income
        birthdate = lo_obj.birthdate
        score = request_score(cpf)

        # Política de Idade
        res = age_policy(birthdate)
        if res:
            return update_loan_order(lo_obj, res)

        # Política de Score
        res = score_policy(cpf)
        if res:
            return update_loan_order(lo_obj, res)

        # Política de Comprometimento
        res = commitment_policy(cpf, terms, amount, score, income)
        if res:
            return update_loan_order(lo_obj, res)
        terms_approved, amount_approved = get_terms_amount_approved(cpf, terms, amount, score, income)
        return update_loan_order(lo_obj, res, amount_approved=amount_approved, terms_approved=terms_approved)


def update_loan_order(loan_order, res, amount_approved=None, terms_approved=None):
    loan_order.status = 'completed'
    if res:
        loan_order.result = 'refused'
        loan_order.refused_policy = res
    else:
        loan_order.result = 'approved'
        loan_order.amount_approved = amount_approved
        loan_order.terms_approved = terms_approved
    return loan_order.save()


def request_score(cpf):
    r = requests.post(url_score, data=json.dumps({"cpf": cpf}), headers=headers)
    r_json = json.loads(r.text)
    return r_json.get('score')


def request_commitment(cpf):
    r = requests.post(url_commitment, data=json.dumps({"cpf": cpf}), headers=headers)
    r_json = json.loads(r.text)
    return r_json.get('commitment')


def age_policy(d1):
    if relativedelta(date.today(), d1).years < 18:
        return 'age'
    return False


def score_policy(cpf):
    if request_score(cpf) < 600:
        return 'score'
    return False


def commitment_policy(cpf, n_installments, amount, score, income):
    commitment = request_commitment(cpf)
    tax = get_tax(n_installments, score) / 100
    approved = False

    while not approved and n_installments <= 12:
        installment_value = calculate_installment(tax, n_installments, amount)
        balance = float(income) * (1 - commitment)

        if balance > installment_value:
            approved = True
        else:
            n_installments += 3

    if not approved:
        return 'commitment'
    return False


def calculate_installment(tax, n_installments, amount):
    return float(amount) * (((1 + tax) ** n_installments * tax) / ((1 + tax) ** n_installments - 1))


def get_tax(n_installments, score):
    if score >= 900:
        if n_installments == 6:
            return 3.9
        elif n_installments == 9:
            return 4.2
        elif n_installments == 12:
            return 4.5
    elif score >= 800:
        if n_installments == 6:
            return 4.7
        elif n_installments == 9:
            return 5.0
        elif n_installments == 12:
            return 5.3
    elif score >= 700:
        if n_installments == 6:
            return 5.5
        elif n_installments == 9:
            return 5.8
        elif n_installments == 12:
            return 6.1
    elif score >= 600:
        if n_installments == 6:
            return 6.4
        elif n_installments == 9:
            return 6.6
        elif n_installments == 12:
            return 6.9
    else:
        return 0.0
    
    
def get_terms_amount_approved(cpf, n_installments, amount, score, income):
    commitment = request_commitment(cpf)
    tax = get_tax(n_installments, score) / 100
    approved = False
    while not approved and n_installments <= 12:
        installment_value = calculate_installment(tax, n_installments, amount)
        balance = float(income) * (1 - commitment)

        if balance > installment_value:
            approved = True
        else:
            n_installments += 3
    if approved:
        return n_installments, installment_value
    return False, False
