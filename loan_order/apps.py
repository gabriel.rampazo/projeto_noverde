from django.apps import AppConfig


class LoanOrderConfig(AppConfig):
    name = 'loan_order'
