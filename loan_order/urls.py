from django.urls import path
from loan_order import views

urlpatterns = [
    path('loan', views.get_post_loan_order),
    path('loan/', views.get_post_loan_order),
    path('loan/<uuid:pk>', views.get_loan_order),
]
