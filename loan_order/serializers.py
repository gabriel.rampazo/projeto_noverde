from loan_order.models import LoanOrder

from rest_framework import serializers


class PostLoanOrderSerializer(serializers.ModelSerializer):
    amount = serializers.DecimalField(max_digits=6, decimal_places=2, min_value=1000.00, max_value=4000.00)
    income = serializers.DecimalField(max_digits=6, decimal_places=2, min_value=1)

    class Meta:
        model = LoanOrder
        fields = ['id', 'name', 'cpf', 'birthdate', 'amount', 'terms', 'income']


class GetLoanOrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = LoanOrder
        fields = ['id', 'status', 'result', 'refused_policy', 'amount_approved', 'terms_approved']

