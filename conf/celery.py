from __future__ import absolute_import
import os
from celery import Celery
from django.conf import settings

SECONDS = 5

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'conf.settings')
app = Celery('celery_app')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings')

app.conf.beat_schedule = {
    'every-5-seconds': {
        'task': 'loan_order.tasks.credit_motor',
        'schedule': SECONDS,
        'args': ()
    }
}

app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))