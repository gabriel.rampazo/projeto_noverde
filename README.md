# Desafio Noverde - Engenheiro de Software Senior

## Introdução
Projeto Desafio Noverde foi desenvolvido em Python com Django, Django Rest Framework, Celery e Redis. O projeto foi concluído conforme todas as expecificações determinadas em https://github.com/noverde/challenge. Desenvolvido por Gabriel Marcolino Rampazo.

## Instalação
Para que esse projeto execute é necessário instalar:

* Python (3.6.3) 
* Redis Server

Linux (Ubuntu):
```
 sudo apt install redis-server
```

Após a instalação do Python, instale os componentes que já estão no arquivo requirements.txt na pasta raiz do projeto.

```
 pip install -r requirements.txt
```

Dica: é interessante fazer um *virtual environment* com o Python 3.6.3, assim pode-se instalar todos os componentes Python sem ocasionar problemas com os demais instalados.
## Execução
Para o processo de execução há três processos que precisam ser iniciados:

* Aplicação Django (Na pasta raiz do projeto):

```
 python manage.py migrate # Cria banco de dados e as tabelas
 python manage.py runserver
```

Rodando este processo já é possível fazer as requisições de POST e GET, para isso use o endereço `http://127.0.0.1:8000/loan`.

* Celery Worker (Na pasta raiz do projeto):

```
 celery -A conf worker -l info
```

Rodando este comando iniciamos o serviço de filas de processo, gerenciado pelo Celery. 
 
* Celery Beat (Na pasta raiz do projeto):

```
 celery -A conf beat -l info
```

Rodando este comando iniciamos o serviço de agendamento de tarefas, onde está programado para executar o motor de crédito da aplicação. 

## Como Funciona

Tudo se inicia por uma requisição do tipo POST feita no endereço `http://127.0.0.1:8000/loan`, enviando seus respectivos campos para um pedido de empréstimo, os dados são validados. Caso haja alguma inconsistência retorna `400 BAD REQUEST` com os erros encontrados. Se caso não houver inconsistências o pedido de empréstimo é gravado no sistema com o status `processing`, e retorna `201 CREATED` com o seu id (UIDD).

Exemplo de body (Envio):
```
 {
    "name": "João da Silva",
    "cpf": "221.245.180-62",
    "birthdate": "1988-05-15",
    "amount": 2000.00,
    "terms": 12,
    "income": 2500.00
 }
```

Caso a requisição for válida e for gravada no sistema, o pedido de empréstimo vai para uma fila para ser validado pelas políticas determinadas pela NoVerde. A fila está programada para processar um pedido com status em `processing` a cada cinco segundos, após o processamento é determinado se o pedido foi aprovado ou recusado, os dados de resposta são gravados no pedido.

Para consultar qual é o status do pedido precisa-se fazer uma requisição do tipo GET feita no endereço `http://127.0.0.1:8000/loan/:id`, com o mesmo id que retornou da requisição POST feita para o pedido.

## Testes

Testes unitários (Na pasta do raiz do projeto):

```
 python manage.py test
```
